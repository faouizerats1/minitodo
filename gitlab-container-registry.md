Create new Deploy Token created in https://gitlab.com/lionelmace/minitodo/-/settings/repository

```
export REGISTRY_USERNAME=
```
```
export REGISTRY_PASSWORD=
```

```
docker login registry.gitlab.com
```

```
docker build -t registry.gitlab.com/lionelmace/minitodo .
```

```
docker push registry.gitlab.com/lionelmace/minitodo
```

```
echo -n "$REGISTRY_USERNAME:$REGISTRY_PASSWORD" | base64
```

```
cat .dockerconfigjson | base64
```

```
kubectl apply -f iks/my-registry-credentials.yml
```

```
kubectl apply -f iks/default.service-account.yml
```

```
kubectl apply -f iks/my-deploy-app.yml
```

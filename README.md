# Introduction

This web app built with a CLEAN stack (CLoudant NoSQL DB, Express, Angular and Node.js) is ready to be deployed on ICP (IBM Cloud Platform).

![Todo](./images/screenshot.png)

This [tutorial](https://lionelmace.github.io/iks-lab) will show you in details how to deploy this step by step on IKS (IBM Cloud Kubernetes Service).

1. Go to https://gitlab.com/lionelmace/minitodo/-/settings/repository
